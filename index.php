<!DOCTYPE html>
<html>
<head>
    <title>dimechan</title>

    <script src="https://unpkg.com/htmx.org@0.0.4"></script>
    <script src="https://unpkg.com/autosize@4.0.2/dist/autosize.js"></script>    

    <link rel="stylesheet" href="styles.css" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
</head>

<body id="body">

    <div class="limited">

    <a href="/"><img src="logo.png" style="width: 100%"/></a>

    <?php
        $redis = new Redis();
        $redis->connect('127.0.0.1', 6379);

        $thread_id = $_GET['thread_id'];

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $post_id = $redis->incr('last_post_id');
            $thread_id = $thread_id ?? $post_id;

            $text = $_POST['text'];

            $length = strlen($text);

            if ($length > 5000) {
                echo "Too long!";
                return;
            }

            $redis->rpush("thread:$thread_id", $post_id);

            $redis->hmset(
                $post_id, [
                'text' => $text,
                'date' => time(),
                'thread' => $thread_id
            ]);

            preg_match_all("/>>(\d*)/", $text, $references);

            forEach($references[1] as $reference) {
                $redis->rpush("replies:$reference", $post_id);
            }

            header("Location: /?thread_id=$thread_id");
        }
        
        if ($thread_id) {
            echo "<div class='thread'>";

            $thread = $redis->lrange("thread:$thread_id", 0, -1);
            
            forEach($thread as $id) {
                $post = $redis->hgetall($id);
                $n_replies = $redis->llen("replies:$id");
                include("post.php");
            }
    
            echo "</div>";

        } else {
            $last = $redis->get('last_post_id');

            $threads = [];

            for($id = $last; $id > $last - 100; $id--) {
                $post = $redis->hgetall($id);

                if ($post) {
                    $post['id'] = $id;
                    $threads[$post['thread']][] = $post;
                } else {
                    break;
                }
            }

            forEach($threads as $thread_id=>$thread) {
                echo "<div class='thread'>";

                $thread_length = count($thread);

                $omitted = $redis->llen('thread:' . $thread_id) - $thread_length;

                // Destructive Reverse?
                $thread = array_reverse($thread);
                
                if ($thread_id == $thread[0]['thread']) {
                    $post = $thread[0];
                    unset($thread[0]);

                } else {
                    $post = $redis->hgetall($thread_id);
                }

                $id = $post['id'];
                $n_replies = $redis->llen('replies:' . $id);
                include("post.php");

                echo "<div class='post' style='margin-bottom: 20px'><a href='/?thread_id=$thread_id'>$omitted posts omitted.</a></div>";

                forEach($thread as $post) {
                    $id = $post['id'];
                    $n_replies = $redis->llen('replies:' . $id);
                    include("post.php");
                }
                echo "</div>";
            }    
        }    
    ?>

    </div>

    <div id="footer" style="position:fixed; bottom: 0; padding-bottom: 20px; z-index: 10; background-color: white;">
        <div class="limited">
            <form id="form" method="POST" onsubmit="localStorage.setItem('text', '');">
                <textarea name="text" id="textbox" onchange="localStorage.setItem('text', textbox.value)"></textarea>
                <button>Post</button>
            </form>
        </div>
    </div>

    <script>
        autosize(document.querySelectorAll('textarea'));
        textbox.value = localStorage.getItem('text') || ""
    </script>

    <script src="scripts.js"></script>


</body>
</html>