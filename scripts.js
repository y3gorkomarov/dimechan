function reference(e, id) {
    e.preventDefault()
    var selection = window.getSelection().toString()
    console.log(!!selection)
    if (selection) {
        var quote = '> ' + selection
    }
    var reference = '\r\n' + '>>' + id + '\r\n' + (quote || "")
    textbox.value += reference
    autosize.update(document.querySelectorAll('textarea'));

    return false
}

function adjust_form_width() {
    footer.style.width = body.offsetWidth + "px"
}

window.onload = adjust_form_width
window.onresize = adjust_form_width

form.addEventListener("keypress", (e) => {
    console.log(e)
    if (e.key == 'Enter' && e.ctrlKey) {
        localStorage.setItem('text', '');
        form.submit()
    }
})