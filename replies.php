<?php
    $redis = new Redis();
    $redis->connect('127.0.0.1', 6379);

    $id = $_GET['id'];

    echo "<div class='thread' id='id$id' hx-swap-oob='true'>";

    $reply_ids = $redis->lrange("replies:$id", 0, -1);

    forEach($reply_ids as $reply_id) {
        $post = $redis->hgetall($reply_id);
        if ($post) {
            $id = $reply_id;
            $n_replies = $redis->llen("replies:$reply_id");
            include('post.php');
        }
    }

    echo "</div>"
?>