<div class="post">
	<?= preg_replace(
		[
			"/>>(\d*)/",
			"/\\r\\n/",
			"/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/"
		],
		[
			"<a hx-get='/post_page.php/$1' hx-swap='outerHTML'>$1</a>",
			"<br />",
			"<a href='$0'>$0</a>"
		], htmlspecialchars($post['text']))
	?>
	<div class="line">
		<span class="info">
			<?php
				if ($n_replies) {
					if ($n_replies == 1) {
						echo "<a hx-get='/replies.php?id=$id'>show one reply</a>";
					} else {
						echo "<a hx-get='/replies.php?id=$id'>show $n_replies replies</a>";
					}
				}
			?>
		</span>
		<span class="info">
			<?= "<a onmousedown='reference(event, $id)'>$id</a>" ?>
		</span>
		<span class="info">
			<?= date("Y-m-d H:i", $post['date']) ?>
		</span>
	</div>
<?= "<div id='id$id'></div>" ?>
</div>